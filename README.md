Omni Sylius Banner Plugin
------------

This is a Sylius plugin that enables rendering of banners. There is a 
user interface added to the admin panel through which one can create 
banner zones that contain different configurations of banners. There 
are a set of twig helper functions to help display these banners in the 
front end of your Sylius e-commerce site. An example of such a banner is
shown bellow:

![banner top](docs/img/banner_homepage.png "Header banner")

## Installation

Require it via composer:

```bash
$ composer require omni/sylius-banner-plugin
```

Import banner routes:
 
```
omni_sylius_banner:
  resource: "@OmniSyliusBannerPlugin/Resources/config/admin_routing.yml"
  prefix: /admin
```

Import banner configs:

```
imports:
    - { resource: "@OmniSyliusBannerPlugin/Resources/config/config.yml" }
```

Add `omni_sylius_banner` liip_imagine filter.
 
```
liip_imagine:
    filter_sets:
        omni_sylius_banner: ~
```

Start using the plugin

## Usage

## Creating banners

Before you start adding your banners to the front end of the application,
you first need to create some. For that please go to the admin panel of your 
application. There, in the marketing section of the left sidebar you should
find a link called `Banners`. If you click on it you should see a list of banner
zones like the one depicted in the image here:

![banner zones](docs/img/zones.png "Banner zones")

This list will most likely be empty, so create a new banner zone. It is
the means to structure a set of banners together.

Afterwards you need to create at least one banner position. Click show
positions on your newly created zone and add a new position. After that is 
done, return to your zones page and click `show banners`. You should be 
presented with a list that is similar to this:

![banners](docs/img/banner_list.png "Banners")

Click create new banner and add your new banner:

![banner form](docs/img/banner_edit.png "Banner form")

Here you can see that it is possible to add the publishing times, and
several images for banners. You can use the type option of your images
to change the image that is being displayed by the banner depending
on some custom criteria like locale or currency. Read more on that in the
Banner types and options section.

### Banner rendering

Banner in specific position can be rendered in twig using function `omni_sylius_banner_display` or `omni_sylius_banner_display_collection`
to display all active banners from position (useful for sliders)

e.g:
`{{ omni_sylius_banner_display('position_code') }}`
`{{ omni_sylius_banner_display_collection('position_code_2') }}`

### Banner types and options

When creating banners it is possible to create different images with different image types.
This is useful when, for example, you need to create different images for different locales.
This way all you need to do is to specify the image type when rendering the banner:
 
`{{ omni_sylius_banner_display('menu', {'image': locale}) }}`

> note: these also apply to `omni_sylius_banner_display_collection` 

### Custom Template

Custom template can be passed as third argument e.g.

`{{ nfq_banner_display('position', {}, 'custom/banner.html.twig') }}`

Default template: `@OmniSyliusBannerPlugin/Banner/banner.html.twig`

### Custom Collection Template

Collection has additional fourth argument for single banner template

`{{ omni_sylius_banner_display_collection('position', {}, 'custom/banners.html.twig', 'custom/banner.html.twig') }}`

Default templates: `@OmniSyliusBannerPlugin/Banner/banner_collection.html.twig` and `@OmniSyliusBannerPlugin/Banner/banner.html.twig`
