<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Omni\Sylius\BannerPlugin\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Sylius\Component\Core\Model\ImageInterface;
use Sylius\Component\Core\Model\TaxonInterface;
use Sylius\Component\Core\Model\ProductInterface;
use Sylius\Component\Resource\Model\TimestampableTrait;

/**
 * Class Banner.
 */
class Banner implements BannerInterface
{
    use TimestampableTrait;

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $code;

    /**
     * @var Collection|ImageInterface[]
     */
    private $images;

    /**
     * @var bool
     */
    private $enabled = true;

    /**
     * @var \DateTime
     */
    private $publishFrom;

    /**
     * @var \DateTime
     */
    private $publishTo;

    /**
     * @var BannerPosition;
     */
    private $position;

    /**
     * Banner constructor.
     */
    public function __construct()
    {
        $this->images = new ArrayCollection();
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getCode(): ?string
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode(?string $code): void
    {
        $this->code = $code;
    }

    /**
     * {@inheritdoc}
     */
    public function getImages(): Collection
    {
        return $this->images;
    }

    /**
     * {@inheritdoc}
     */
    public function getImageByCode(string $code): Collection
    {
        foreach ($this->images as $image) {
            if ($code === $image->getCode()) {
                return $image;
            }
        }

        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function hasImages(): bool
    {
        return !$this->images->isEmpty();
    }

    /**
     * {@inheritdoc}
     */
    public function hasImage(ImageInterface $image): bool
    {
        return $this->images->contains($image);
    }

    /**
     * {@inheritdoc}
     */
    public function addImage(ImageInterface $image): void
    {
        if (false === $this->hasImage($image)) {
            $image->setOwner($this);
            $this->images->add($image);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function removeImage(ImageInterface $image): void
    {
        $image->setOwner(null);
        $this->images->removeElement($image);
    }

    /**
     * {@inheritdoc}
     */
    public function getImage()
    {
        if (!$this->hasImages()) {
            return null;
        }

        return $this->getImages()->first();
    }

    /**
     * {@inheritdoc}
     */
    public function getImagesByType(string $type): Collection
    {
        $images = [];

        foreach ($this->images as $image) {
            if ($type === $image->getType()) {
                $images[] = $image;
            }
        }

        return $images;
    }

    /**
     * {@inheritdoc}
     */
    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    /**
     * {@inheritdoc}
     */
    public function setEnabled(?bool $enabled): void
    {
        $this->enabled = (bool)$enabled;
    }

    /**
     * {@inheritdoc}
     */
    public function enable(): void
    {
        $this->enabled = true;
    }

    /**
     * {@inheritdoc}
     */
    public function disable(): void
    {
        $this->enabled = false;
    }

    /**
     * {@inheritdoc}
     */
    public function getPublishFrom()
    {
        return $this->publishFrom;
    }

    /**
     * {@inheritdoc}
     */
    public function setPublishFrom($publishFrom)
    {
        $this->publishFrom = $publishFrom;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getPublishTo()
    {
        return $this->publishTo;
    }

    /**
     * {@inheritdoc}
     */
    public function setPublishTo($publishTo)
    {
        $this->publishTo = $publishTo;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function isPublished(\DateTime $date = null)
    {
        if (!$this->isEnabled()) {
            return false;
        }

        if (null === $date) {
            $date = new \DateTime();
        }

        $start = $this->getPublishFrom();
        $end = $this->getPublishTo();

        return (null === $start || $start >= $date) && (null === $end || $end <= $date);
    }

    /**
     * {@inheritdoc}
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * {@inheritdoc}
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }
}
