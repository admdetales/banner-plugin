<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Omni\Sylius\BannerPlugin\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Sylius\Component\Resource\Model\CodeAwareInterface;
use Sylius\Component\Resource\Model\ResourceInterface;

/**
 * Class BannerZone.
 */
interface BannerZoneInterface extends ResourceInterface, CodeAwareInterface
{
    /**
     * @return string
     */
    public function getTitle(): ?string;

    /**
     * @param string $title
     *
     * @return BannerZoneInterface
     */
    public function setTitle($title): BannerZoneInterface;

    /**
     * @return Collection|BannerPosition[]
     */
    public function getPositions();

    /**
     * @param BannerPosition $position
     *
     * @return BannerZoneInterface
     */
    public function addPosition(BannerPosition $position): BannerZoneInterface;

    /**
     * @param BannerPosition $position
     *
     * @return BannerZoneInterface
     */
    public function removePosition(BannerPosition $position): BannerZoneInterface;
}
