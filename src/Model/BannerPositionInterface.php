<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Omni\Sylius\BannerPlugin\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Sylius\Component\Resource\Model\CodeAwareInterface;
use Sylius\Component\Resource\Model\ResourceInterface;

interface BannerPositionInterface extends ResourceInterface, CodeAwareInterface
{
    /**
     * @return BannerZone
     */
    public function getZone();

    /**
     * @param BannerZone $zone
     *
     * @return $this
     */
    public function setZone(BannerZone $zone);

    /**
     * @return string
     */
    public function getTitle();
    
    /**
     * @param string $title
     *
     * @return $this
     */
    public function setTitle($title);

    /**
     * @return Collection|Banner[]
     */
    public function getBanners();

    /**
     * @param Banner $banner
     *
     * @return $this
     */
    public function addBanner(Banner $banner);

    /**
     * @param Banner $banner
     *
     * @return $this
     */
    public function removeBanner(Banner $banner);

    /**
     * @return int
     */
    public function getWidth();

    /**
     * @param int $width
     *
     * @return $this
     */
    public function setWidth($width);

    /**
     * @return int
     */
    public function getHeight();

    /**
     * @param int $height
     *
     * @return $this
     */
    public function setHeight($height);
}
