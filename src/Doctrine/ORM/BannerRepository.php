<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Omni\Sylius\BannerPlugin\Doctrine\ORM;

use DateTime;
use DateTimeInterface;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Omni\Sylius\BannerPlugin\Model\Banner;
use Omni\Sylius\BannerPlugin\Model\BannerZone;
use Sylius\Bundle\ResourceBundle\Doctrine\ORM\EntityRepository;

/**
 * Class BannerRepository.
 */
class BannerRepository extends EntityRepository
{
    /**
     * @param string $zone
     *
     * @return QueryBuilder
     */
    public function createByZoneCodeQueryBuilder(string $zone): QueryBuilder
    {
        $builder = $this->createQueryBuilder('o');
        $builder
            ->select('o', 'position', 'zone')
            ->innerJoin('o.position', 'position')
            ->innerJoin('position.zone', 'zone', Join::WITH, $builder->expr()->eq('zone.code', ':zone'))
            ->setParameter('zone', $zone);

        return $builder;
    }

    /**
     * @param BannerZone        $zone
     * @param DateTimeInterface $date
     *
     * @return Banner[]
     */
    public function findForZone(BannerZone $zone, DateTimeInterface $date = null): array 
    {
        if (null === $date) {
            $date = new DateTime();
        }

        $builder = $this->createQueryBuilder('o');
        $builder
            ->addSelect('image')
            ->distinct(true)
            ->innerJoin('o.position', 'position')
            ->innerJoin('position.zone', 'zone', Join::WITH, $builder->expr()->eq('zone', ':zone'))
            ->innerJoin('o.images', 'image', Join::WITH, $builder->expr()->isNotNull('image.path'))
            ->where(
                $builder->expr()->orX(
                    $builder->expr()->lte('o.publishFrom', ':date'),
                    $builder->expr()->isNull('o.publishFrom')
                ),
                $builder->expr()->orX(
                    $builder->expr()->gte('o.publishTo', ':date'),
                    $builder->expr()->isNull('o.publishFrom')
                ),
                $builder->expr()->eq('o.enabled', $builder->expr()->literal(true))
            )
            ->orderBy('o.updatedAt', 'asc')
            ->setParameter('date', $date)
            ->setParameter('zone', $zone);

        return $builder->getQuery()->getResult();
    }
}
