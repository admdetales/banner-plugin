<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Omni\Sylius\BannerPlugin\Doctrine\ORM;

use Doctrine\ORM\Query\Expr\Join;
use Omni\Sylius\BannerPlugin\Model\BannerZone;
use Sylius\Bundle\ResourceBundle\Doctrine\ORM\EntityRepository;

class BannerZoneRepository extends EntityRepository
{
    /**
     * @param string $code
     *
     * @return null|BannerZone|object
     */
    public function findOneByCode(string $code): ?BannerZone
    {
        return $this->findOneBy(['code' => $code]);
    }

    /**
     * @param string $code
     *
     * @return BannerZone
     */
    public function findOneByPositionCode(string $code): ?BannerZone
    {
        $builder = $this->createQueryBuilder('zone');
        $builder
            ->addSelect('position')
            ->innerJoin('zone.positions', 'p', Join::WITH, $builder->expr()->eq('p.code', ':code'))
            ->innerJoin('zone.positions', 'position')
            ->setParameter('code', $code);

        return $builder->getQuery()->getSingleResult();
    }
}
