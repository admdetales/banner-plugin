<?php

namespace Omni\Sylius\BannerPlugin\Form\Subscriber;

use Omni\Sylius\BannerPlugin\Model\BannerPositionInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class AddZoneSubscriber implements EventSubscriberInterface
{
    public static function getSubscribedEvents()
    {
        return [
            FormEvents::SUBMIT => 'onSubmit',
        ];
    }

    public function onSubmit(FormEvent $event)
    {
        /** @var BannerPositionInterface $data */
        $data = $event->getData();
        $zone = $event->getForm()->getConfig()->getAttribute('data_collector/passed_options')['zone'];

        $data->setZone($zone);
    }
}
