<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Omni\Sylius\BannerPlugin\Twig;

use Omni\Sylius\BannerPlugin\Finder\BannerFinder;
use Twig_Environment as Environment;
use Twig_Extension as Extension;
use Twig_SimpleFunction as SimpleFunction;

/**
 * Class BannerExtension.
 */
class BannerExtension extends Extension
{
    /**
     * @var BannerFinder
     */
    private $finder;

    /**
     * BannerExtension constructor.
     *
     * @param BannerFinder $finder
     */
    public function __construct(BannerFinder $finder)
    {
        $this->finder = $finder;
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return [
            new SimpleFunction(
                'omni_sylius_banner_display',
                [$this, 'display'],
                [
                    'is_safe' => ['html'],
                    'needs_environment' => true,
                ]
            ),
            new SimpleFunction(
                'omni_sylius_banner_display_collection',
                [$this, 'displayCollection'],
                [
                    'is_safe' => ['html'],
                    'needs_environment' => true,
                ]
            ),
        ];
    }

    /**
     * It is expected for banners in same zone to have same context during same request.
     *
     * @param Environment $environment
     * @param string      $position
     * @param array       $options
     * @param string      $template
     *
     * @return string
     */
    public function display(
        Environment $environment,
        $position,
        array $options = [],
        $template = '@OmniSyliusBannerPlugin/Banner/banner.html.twig'
    ) {
        return $environment->render(
            $template,
            [
                'banner' => $this->finder->findForPosition($position, $options),
                'options' => $options,
            ]
        );
    }

    /**
     * It is expected for banners in same zone to have same context during same request.
     *
     * @param Environment $environment
     * @param string      $position
     * @param array       $options
     * @param string      $template
     * @param string      $entryTemplate
     *
     * @return string
     */
    public function displayCollection(
        Environment $environment,
        $position,
        array $options = [],
        $template = '@OmniSyliusBannerPlugin/Banner/banner_collection.html.twig',
        $entryTemplate = '@OmniSyliusBannerPlugin/Banner/banner.html.twig'
    ) {
        $this->finder->findAllForPosition($position, $options);

        return $environment->render(
            $template,
            [
                'banners' => $this->finder->findAllForPosition($position, $options),
                'template' => $entryTemplate,
                'options' => $options,
            ]
        );
    }
}
