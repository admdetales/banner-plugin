<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\BannerPlugin\Fixture\Factory;

use Doctrine\ORM\EntityRepository;
use Omni\Sylius\BannerPlugin\Model\BannerZoneInterface;
use Omni\Sylius\BannerPlugin\Model\BannerZoneTranslationInterface;
use Sylius\Bundle\CoreBundle\Fixture\Factory\AbstractExampleFactory;
use Sylius\Bundle\CoreBundle\Fixture\Factory\ExampleFactoryInterface;
use Sylius\Component\Locale\Model\LocaleInterface;
use Sylius\Component\Resource\Factory\FactoryInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BannerZoneExampleFactory extends AbstractExampleFactory implements ExampleFactoryInterface
{
    /**
     * @var FactoryInterface
     */
    private $bannerZoneFactory;

    /**
     * @var EntityRepository
     */
    private $localeRepository;

    /**
     * @var OptionsResolver
     */
    private $optionsResolver;

    /**
     * @param FactoryInterface $bannerZoneFactory
     * @param EntityRepository $localeRepository
     */
    public function __construct(FactoryInterface $bannerZoneFactory, EntityRepository $localeRepository)
    {
        $this->bannerZoneFactory = $bannerZoneFactory;
        $this->localeRepository = $localeRepository;
        $this->optionsResolver = new OptionsResolver();

        $this->configureOptions($this->optionsResolver);
    }

    /**
     * {@inheritdoc}
     */
    public function create(array $options = []): BannerZoneInterface
    {
        $options = $this->optionsResolver->resolve($options);

        /** @var BannerZoneInterface $zone */
        $zone = $this->bannerZoneFactory->createNew();

        $zone->setCode($options['code']);

        /** @var LocaleInterface $locale */
        foreach ($this->localeRepository->findAll() as $locale) {
            /** @var BannerZoneTranslationInterface $translation */
            $translation = $zone->getTranslation($locale->getCode());

            $translation->setTitle($options['title']);
            $translation->setDescription($options['description']);
            $translation->setLink($options['link']);
        }

        return $zone;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureOptions(OptionsResolver $resolver): void
    {
        $resolver
            ->setRequired([
                'code',
                'title',
            ])
            ->setDefault('description', '')
            ->setDefault('link', '')
        ;
    }
}
